ARG BASE_DISTRO=ubuntu
ARG BASE_VERSION=20.04
ARG PACKAGE_MANAGER=apt
ARG N_BUILD_THREADS=4
ARG ANTS_VERSION=2.3.5
ARG ANTS_COMMIT_HASH
ARG ANTS_GIT=https://github.com/ANTsX/ANTs.git
ARG ANTS_BUILD_SHARED=ON
ARG ANTS_BUILD_VTK=OFF

FROM ${BASE_DISTRO}:${BASE_VERSION} as apt-base
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -qq && \
    apt-get install -y -q \
           g++ \
           gcc \
           git \
           make \
           curl \
           libssl-dev \
           zlib1g-dev && \
    rm -rf /var/lib/apt/lists/*

RUN cd /opt && \
    curl -L --silent https://cmake.org/files/v3.20/cmake-3.20.4.tar.gz > cmake-3.20.4.tar.gz && \
    tar -xzf cmake-3.20.4.tar.gz && \
    cd cmake-3.20.4 && \
    ./bootstrap && \
    make && \
    make install && \
    cd /opt && \
    rm -rf cmake-3.20.4


FROM ${PACKAGE_MANAGER}-base as install-ants

# Install Variables
ARG N_BUILD_THREADS
ARG ANTS_VERSION
ARG ANTS_COMMIT_HASH
ARG ANTS_GIT
ARG ANTS_BUILD_SHARED
ARG ANTS_BUILD_VTK

# Install ANTs
RUN git clone ${ANTS_GIT} /tmp/ants-source
RUN cd /tmp/ants-source && \
    if [ -z "${ANTS_VERSION}" ]; then git checkout $ANTS_COMMIT_HASH; else git checkout tags/v$ANTS_VERSION; fi
RUN mkdir /tmp/ants-build && \
    cd /tmp/ants-build && \
    ARCH=$(uname -m) && \
    if [ "$ARCH" = "aarch64" ]; then \
        CMAKE_C_FLAGS="-DPNG_ARM_NEON_OPT=0"; \
    fi && \
    cmake /tmp/ants-source \
        -DCMAKE_INSTALL_PREFIX=/opt/ants \
        -DCMAKE_BUILD_TYPE=Release \
        -DBUILD_SHARED_LIBS=${ANTS_BUILD_SHARED} \
        -DUSE_VTK=${ANTS_BUILD_VTK} \
        -DUSE_SYSTEM_VTK=OFF \
        -DUSE_SYSTEM_ITK=OFF \
        -DSuperBuild_ANTS_USE_GIT_PROTOCOL=OFF \
        -DCMAKE_C_FLAGS=${CMAKE_C_FLAGS} \
        -DBUILD_TESTING=OFF && \
    make -j ${N_BUILD_THREADS} && \
    cd ANTS-build && \
    make install
RUN if [ -z "${ANTS_VERSION}" ]; then \
        BUILD_VERSION=$(LD_LIBRARY_PATH=/opt/ants/lib:$LD_LIBRARY_PATH /opt/ants/bin/antsRegistration --version | grep Version | sed 's/.*:\ //'); \
        BUILD_HASH=${ANTS_COMMIT_HASH}; \
    else \
        BUILD_VERSION=${ANTS_VERSION}; \
        BUILD_HASH=$(cd /tmp/ants-source;git rev-parse HEAD); \
    fi; \
    echo -e "{\
    \n  \"build_version\": \"$BUILD_VERSION\", \
    \n  \"build_commit_hash\": \"$BUILD_HASH\", \
    \n  \"build_shared_libs\": \"$ANTS_BUILD_SHARED\", \
    \n  \"build_shared_vtk\": \"$ANTS_BUILD_VTK\", \
    \n  \"build_git_url\": \"$ANTS_GIT\" \
    \n}" > /opt/ants/manifest.json


FROM ${BASE_DISTRO}:${BASE_VERSION} as final
LABEL maintainer=blake.dewey@jhu.edu
# Copy Build Artifacts
COPY --from=install-ants /opt/ants /opt/ants

# Update Environment Variables
ENV ANTSPATH /opt/ants/bin
ENV PATH ${ANTSPATH}:${PATH}
ENV LD_LIBRARY_PATH=/opt/ants/lib:${LD_LIBRARY_PATH}

CMD ["/bin/bash"]
